var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var mysqlModel = require('mysql-model');

var MyAppModel = mysqlModel.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'Diva2020#',
  database : 'diva_chat',
});


app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))


var Message = MyAppModel.extend({
    tableName: "messages",
});


message = new MyAppModel({tableName: "messages"});


app.get('/messages', (req, res) => {

  	message.find('all', {where: ""}, function(err, rows, fields) {
    	res.send(rows);
	});

})

app.get('/messages/:user', (req, res) => {
  	var user = req.params.user

	message.find({name: user}, {where: ""}, function(err, rows, fields) {
		res.send(rows);
	});

})

app.post('/messages', async (req, res) => {
  try{
    var message = new Message(req.body);

    var savedMessage = message.save()
      console.log('saved');

    
        io.emit('message', req.body);
      res.sendStatus(200);
  }
  catch (error){
    res.sendStatus(500);
    return console.log('error',error);
  }
  finally{
    console.log('Message Posted')
  }

})



io.on('connection', () =>{
  console.log('a user is connected')
})


var server = http.listen(3000, () => {
  console.log('server is running on port', server.address().port);
});