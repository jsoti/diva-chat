CREATE DATABASE IF NOT EXISTS diva_chat;

USE diva_chat;

DROP TABLE IF EXISTS messages;

CREATE TABLE messages (
name TEXT NOT NULL,
message TEXT NOT NULL
);

